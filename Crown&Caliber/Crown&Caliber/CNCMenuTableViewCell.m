//
//  menuTableViewCell.m
//  CrownNCaliber
//
//  Created by Admin on 07.07.14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import "CNCmenuTableViewCell.h"

@implementation CNCMenuTableViewCell

- (void)configureCellWithDictionary:(NSDictionary *)dictionary isSelected:(BOOL)isSelected isFirstItem:(BOOL)isFirstItem {
    self.titleLabel.text = dictionary[@"title"];

    NSString* imageName = dictionary[isSelected ? @"menuImageSelected": @"menuImage"];
    self.menuImage.image  = [imageName length] > 0 ? [UIImage imageNamed:imageName] : nil;

    self.selectedStateView.hidden = !isSelected;

    
    if (isFirstItem || isSelected) {
        self.titleLabel.textColor = [UIColor blackColor];
    }
    else {
        self.titleLabel.textColor = [UIColor colorWithRed:0.47f green:0.47f blue:0.47f alpha:1];
    }

    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

@end
