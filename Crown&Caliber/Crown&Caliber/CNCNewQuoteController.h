//
//  CNCNewQuoteController.h
//  Crown&Caliber
//
//  Created by Admin on 10.07.14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNCMenuViewController.h"

@interface CNCNewQuoteController : UITableViewController

@property (nonatomic, strong) id<CNCMenuDelegate> menuDelegate;

-(void) resetUserData;

@end
