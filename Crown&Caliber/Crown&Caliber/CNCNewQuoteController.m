//
//  CNCNewQuoteController.m
//  Crown&Caliber
//
//  Created by Admin on 10.07.14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import "CNCNewQuoteController.h"
#import "CNCQuote.h"
#import "CNCApi.h"
#import "CNCConfig.h"
#import "UIColor+CNCColor.h"

#import <BlocksKit/UIActionSheet+BlocksKit.h>
#import <BlocksKit/UIImagePickerController+BlocksKit.h>
#import "CNCTextField.h"
#import <MBProgressHUD.h>
#import <JVFloatLabeledTextView.h>
#import <ActionSheetPicker.h>

#define ANIMATION_SPEED 0.5
@interface CNCNewQuoteController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, UITextViewDelegate, UIGestureRecognizerDelegate>


@property (strong, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIImageView *imageFront;
@property (weak, nonatomic) IBOutlet UIImageView *imageBack;

@property (weak, nonatomic) IBOutlet UIView *secondCoverView;
@property (weak, nonatomic) IBOutlet UIView *thirdCoverView;
@property (weak, nonatomic) IBOutlet UIView *thankYouCover;

@property (strong, nonatomic) CNCQuote* currentQuote;

@property (weak, nonatomic) IBOutlet CNCTextField *customerName;
@property (weak, nonatomic) IBOutlet CNCTextField *customerEmail;
@property (weak, nonatomic) IBOutlet CNCTextField *customerPhone;
@property (weak, nonatomic) IBOutlet CNCTextField *watchBrand;
@property (weak, nonatomic) IBOutlet CNCTextField *watchModel;
@property (weak, nonatomic) IBOutlet CNCTextField *watchModelNumber;

@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentUSLocation;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentDocuments;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentModelFound;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentSell;
@property (weak, nonatomic) IBOutlet JVFloatLabeledTextView *additionalInfo;

@property (weak, nonatomic) IBOutlet UIButton *brandPickerButton;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UIButton *secondStepButton;

@property (weak, nonatomic) IBOutlet UILabel *termsLabel;
@property (weak, nonatomic) IBOutlet UILabel *sellLabel;
@property (strong, nonatomic) UITapGestureRecognizer *termsTap;

@property (weak, nonatomic) IBOutlet UISwitch *agreeSwitch;


@property (weak, nonatomic) IBOutlet CNCTextField *retailerName;
@property (weak, nonatomic) IBOutlet UISegmentedControl *referredByRetailerSegmentedControl;

@property (nonatomic) CGRect documentsSegementFrame;
@property (nonatomic) CGRect sellLabelFrame;
@property (nonatomic) CGRect sellSegmentFrame;
@property (nonatomic) CGRect nextButtonFrame;
@property (nonatomic) CGRect submitButtonFrame;

@end

@implementation CNCNewQuoteController

typedef NS_ENUM(NSUInteger, CNCSecondStepField){
    CNCCustomerName = 1,
    CNCCustomerEmail,
    CNCCustomerPhone,
    CNCWhatchBrand,
    CNCWhatchModel,
    CNCWhatchModelNumber,
    CNCRetailerName
};


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.tableHeaderView = self.headerView;
    
    self.additionalInfo.placeholder = @"Additional information";
    
    
    self.termsTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(termsLabelTap)];
    self.termsTap.numberOfTapsRequired = 1;
    self.termsTap.delegate = self;
    [self.termsLabel addGestureRecognizer:self.termsTap];
    
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:@"I am over 18 and agree to the terms & conditions"];
    
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor blueColor] range:NSMakeRange(30  ,18)];
    [string  addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInt:NSUnderlineStyleSingle] range:NSMakeRange(30, 18)];
    
    self.termsLabel.attributedText = string;
}

- (void) viewDidAppear: (BOOL) animated
{
	NSDictionary* userInfo = [[NSUserDefaults standardUserDefaults] objectForKey: UserInfoKey];
	if ([userInfo isKindOfClass: [NSDictionary class]])
	{
		self.currentQuote.name = _customerName.text =
		[userInfo[UserNameKey] isKindOfClass: [NSString class]] ? userInfo[UserNameKey] : @"";
		
		self.currentQuote.email = _customerEmail.text =
		[userInfo[UserEmailKey] isKindOfClass: [NSString class]] ? userInfo[UserEmailKey] : @"";
		
		self.currentQuote.phone = _customerPhone.text =
		[userInfo[UserPhoneNumberKey] isKindOfClass: [NSString class]] ? userInfo[UserPhoneNumberKey] : @"";
	}
	[super viewDidAppear: animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


- (IBAction)imageFrontPickAction: (id) sender
{
	[self showDialogForChosingSourceForView: sender];
}

- (void) showDialogForChosingSourceForView: (id) sender
{
	NSLog(@"Version %i", __IPHONE_OS_VERSION_MAX_ALLOWED);
	UIImageView* imageView =  (UIImageView*) ((UITapGestureRecognizer*)sender).view;
	[self pickImageForVIew: imageView completion:^(UIImage *resultImage) {
		if (imageView == self.imageFront)
		{
			self.currentQuote.imageFront = resultImage;
		}
		else
		{
			self.currentQuote.imageBack = resultImage;
		}
		imageView.image = resultImage;
		
		[self checkSecondStep: self.currentQuote];
	}];
}

- (IBAction)imageBackPickAction:(id)sender {
    [self showDialogForChosingSourceForView: sender];
}

- (void) pickImage: (void (^)(UIImage *resultImage))success
{
	[self pickImageForVIew: nil completion: success];
}

- (void) pickImageForVIew: (UIImageView*) imaegView
			   completion: (void (^)(UIImage *resultImage))success
{
	if ([UIAlertAction class])
	{
		if (imaegView)
		{
			UIAlertController *actionSheetController = [UIAlertController alertControllerWithTitle: nil message: nil preferredStyle: UIAlertControllerStyleActionSheet];
			
			UIAlertAction *camera = [UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
									 {
										 if (![UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
										 {
											 return;
										 }
										 UIImagePickerController* imagePickerController =  [[UIImagePickerController alloc] init];
										 [imagePickerController  setSourceType:UIImagePickerControllerSourceTypeCamera];
										 imagePickerController.delegate = self;
										 
										 [imagePickerController setBk_didFinishPickingMediaBlock:^(UIImagePickerController *picker, NSDictionary *info) {
											 [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
											 UIImage* resultImage = [info objectForKey:UIImagePickerControllerOriginalImage];
											 
											 [picker dismissViewControllerAnimated:YES completion:nil];
											 
											 if (success) {
												 success(resultImage);
											 }
										 }];
										 
										 [self presentViewController:imagePickerController animated:YES completion:NULL];
									 }];
			
			UIAlertAction *library = [UIAlertAction actionWithTitle:@"Library" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action)
									  {
										  UIImagePickerController* imagePickerController =  [[UIImagePickerController alloc] init];
										  [imagePickerController  setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
										  
										  imagePickerController.delegate = self;
										  [imagePickerController setBk_didFinishPickingMediaBlock:^(UIImagePickerController *picker, NSDictionary *info) {
											  [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
											  
											  UIImage* resultImage = [info objectForKey:UIImagePickerControllerOriginalImage];
											  [picker dismissViewControllerAnimated:YES completion:nil];
											  
											  if (success) {
												  success(resultImage);
											  }
										  }];
										  
										  [self presentViewController:imagePickerController animated:YES completion:NULL];
									  }];
			
			[actionSheetController addAction: camera];
			[actionSheetController addAction: library];
			
			actionSheetController.view.tintColor = [UIColor blackColor];
			
			[actionSheetController setModalPresentationStyle: UIModalPresentationPopover];
			
			UIPopoverPresentationController *popPresenter = [actionSheetController
															 popoverPresentationController];
			popPresenter.sourceView = imaegView;
			popPresenter.sourceRect = imaegView.bounds;
			[self presentViewController: actionSheetController animated:YES completion:nil];
		}
	}
	else
	{
		UIActionSheet *imagePickAction = [UIActionSheet bk_actionSheetWithTitle:nil];
		
		[imagePickAction bk_addButtonWithTitle:@"Camera" handler:^{
			
			UIImagePickerController* imagePickerController =  [[UIImagePickerController alloc] init];
			[imagePickerController  setSourceType:UIImagePickerControllerSourceTypeCamera];
			imagePickerController.delegate = self;
			
			[imagePickerController setBk_didFinishPickingMediaBlock:^(UIImagePickerController *picker, NSDictionary *info) {
				[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
				UIImage* resultImage = [info objectForKey:UIImagePickerControllerOriginalImage];
				
				[picker dismissViewControllerAnimated:YES completion:nil];
				
				if (success) {
					success(resultImage);
				}
			}];
			
			[self presentViewController:imagePickerController animated:YES completion:NULL];
		}];
		
		[imagePickAction bk_addButtonWithTitle:@"Library" handler:^{
			UIImagePickerController* imagePickerController =  [[UIImagePickerController alloc] init];
			[imagePickerController  setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
			
			imagePickerController.delegate = self;
			[imagePickerController setBk_didFinishPickingMediaBlock:^(UIImagePickerController *picker, NSDictionary *info) {
				[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
				
				UIImage* resultImage = [info objectForKey:UIImagePickerControllerOriginalImage];
				[picker dismissViewControllerAnimated:YES completion:nil];
				
				if (success) {
					success(resultImage);
				}
			}];
			
			[self presentViewController:imagePickerController animated:YES completion:NULL];
			
		}];
		
		[imagePickAction bk_setCancelButtonWithTitle:@"Cancel" handler:^{
			[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
		}];
		
		[imagePickAction showInView:self.view];
	}
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)locationAction:(UISegmentedControl *)sender {
    self.currentQuote.isDomestic = sender.selectedSegmentIndex == 0;
}



- (IBAction)modelFoundAction:(UISegmentedControl *)sender {
    [self.view endEditing:YES];
    
    self.currentQuote.hasModelNumber = sender.selectedSegmentIndex == 0;
    
    if (sender.selectedSegmentIndex == 0) {
        self.watchModel.returnKeyType = UIReturnKeyNext;

        [UIView animateWithDuration:ANIMATION_SPEED animations:^{
            self.watchModelNumber.hidden = NO;
            self.segmentDocuments.frame = self.documentsSegementFrame;
            self.secondStepButton.frame = self.nextButtonFrame;
			self.sellLabel.frame = self.sellLabelFrame;
			self.segmentSell.frame = self.sellSegmentFrame;
        }];

    }
    else {
        [UIView animateWithDuration:ANIMATION_SPEED animations:^{
            self.nextButtonFrame = self.secondStepButton.frame;
            self.documentsSegementFrame = self.segmentDocuments.frame;
			self.sellLabelFrame = self.sellLabel.frame;
			self.sellSegmentFrame = self.segmentSell.frame;

			float yOffset = 80.0;
            
            CGRect nextButtonFrame = self.secondStepButton.frame;
            nextButtonFrame.origin.y -= yOffset;
            self.secondStepButton.frame = nextButtonFrame;
            
            CGRect newFrame = self.segmentDocuments.frame;
            newFrame.origin.y -= yOffset;
            self.segmentDocuments.frame = newFrame;

			CGRect sellLabelFrame = self.sellLabel.frame;
			sellLabelFrame.origin.y -= yOffset;
			self.sellLabel.frame = sellLabelFrame;

			CGRect sellSegmentFrame = self.segmentSell.frame;
			sellSegmentFrame.origin.y -= yOffset;
			self.segmentSell.frame = sellSegmentFrame;

			
            self.watchModelNumber.hidden = YES;
        }];
        

        self.watchModel.returnKeyType = UIReturnKeyDone;
    }
    
    [self checkThirdStep:self.currentQuote];
}

- (IBAction)retailerAction :(UISegmentedControl *)sender
{
    if (sender.selectedSegmentIndex == 0)
    {
        [self.retailerName setHidden:NO];
        self.currentQuote.hasRetailer = YES;
    }
    else
    {
        [self.retailerName setHidden:YES];
        self.currentQuote.hasRetailer = NO;
    }
    [self checkSubmitButton:self.currentQuote];

}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    switch (textField.tag) {
        case CNCCustomerName:
            [self.customerEmail becomeFirstResponder];
            break;
        case CNCCustomerEmail:
            [self.customerPhone becomeFirstResponder];
            break;
        case CNCCustomerPhone:
            [self showBrandPicker:self.brandPickerButton];
            break;
        case CNCWhatchModel:
            if (self.segmentModelFound.selectedSegmentIndex == 0) {
                [self.watchModelNumber becomeFirstResponder];
            }
            else {
                [self.view endEditing:YES];
            }


            break;
        case CNCWhatchModelNumber:
            [self.view endEditing:YES];
            
            break;
        case CNCRetailerName:
            [self.view endEditing:YES];
            break;

        default:
            break;
    }
    
    return YES;
}

- (IBAction)textChanged:(JVFloatLabeledTextField *)sender {
  switch (sender.tag) {
    case CNCCustomerName:
      self.currentQuote.name = sender.text;
      break;
    case CNCCustomerEmail:
      self.currentQuote.email = sender.text;
      break;
    case CNCCustomerPhone:
      sender.text = sender.text.length > 15? [sender.text substringToIndex:15]:sender.text;
      self.currentQuote.phone = sender.text;
      break;
    case CNCWhatchBrand:
      self.currentQuote.brand = sender.text;
      break;
    case CNCWhatchModel:
      self.currentQuote.modelName = sender.text;
      break;
    case CNCWhatchModelNumber:
      self.currentQuote.modelNumber = sender.text;
      break;
    case CNCRetailerName:
      self.currentQuote.retailerName = sender.text;
      break;
    default:
      break;
  }
  
  [self checkThirdStep:self.currentQuote];
  [self checkSubmitButton:self.currentQuote];

}

- (IBAction)textFieldDidEndEditing:(JVFloatLabeledTextField *)sender {
    switch (sender.tag) {
        case CNCCustomerName:
            sender.layer.borderColor = [self validateName:sender.text]? [UIColor CNCGrayColor].CGColor : [UIColor redColor].CGColor;
            break;
        case CNCCustomerEmail:
            sender.layer.borderColor = [self validateEmail:sender.text]? [UIColor CNCGrayColor].CGColor : [UIColor redColor].CGColor;
            break;
        case CNCCustomerPhone:
            sender.layer.borderColor = [self validatePhone:sender.text]? [UIColor CNCGrayColor].CGColor : [UIColor redColor].CGColor;
            break;
        case CNCWhatchModel:
            sender.layer.borderColor = [self validateModelName:sender.text]? [UIColor CNCGrayColor].CGColor : [UIColor redColor].CGColor;
            break;
        case CNCWhatchModelNumber:
            sender.layer.borderColor = [self validateModelNumber:sender.text]? [UIColor CNCGrayColor].CGColor : [UIColor redColor].CGColor;
            break;
            
        default:
            break;
    }
    
}


#pragma mark - Internal logic

- (void)checkSecondStep:(CNCQuote*)quote {
    if (self.imageBack.image && self.imageFront.image) {
        self.secondCoverView.hidden = YES;
    }
}

- (void)checkSubmitButton:(CNCQuote*)quote {
    
    if (self.agreeSwitch.isOn&& ((self.referredByRetailerSegmentedControl.selectedSegmentIndex == 0)? (quote.retailerName.length > 0):YES) && ([self validateName:quote.name] && [self validatePhone:quote.phone] && [self validateEmail:quote.email] && quote.brand.length > 0 && [self validateModelName:quote.modelName]  && (quote.hasModelNumber? [self validateModelNumber:quote.modelNumber]:YES))) {
        self.submitButton.enabled = YES;

    }
    else
    {
        self.submitButton.enabled = NO;
    }

}

- (IBAction)segmentDocumentsAcion:(UISegmentedControl *)sender
{
	NSString* documentsParam = nil;
	switch (sender.selectedSegmentIndex)
	{
		case 0:
			documentsParam = @"Box Only";
			break;
		case 1:
			documentsParam = @"Papers Only";
			break;
		case 2:
			documentsParam = @"Box & Papers";
			break;
		case 3:
			documentsParam = @"Neither";
			break;

		default:
			break;
	}
    self.currentQuote.documents = documentsParam;
	[self checkThirdStep: self.currentQuote];
}

-(IBAction)sellSegmentedControlChanged :(id)sender
{
    UISegmentedControl *segmentedControl = sender;

    switch (segmentedControl.selectedSegmentIndex)
    {
        case 0:
            self.currentQuote.isFullQuote = YES;

        case 1:
            self.currentQuote.isFullQuote = NO;
    }
}


- (void)checkThirdStep:(CNCQuote*)quote {
	BOOL condition =  self.segmentDocuments.selectedSegmentIndex != UISegmentedControlNoSegment && ([self validateName:quote.name] && [self validateEmail:quote.email] && [self validatePhone:quote.phone] && quote.brand.length > 0 && (quote.hasModelNumber? [self validateModelNumber:quote.modelNumber]:YES));
	
    if (condition) {
		NSDictionary* userInfo = @{UserNameKey : quote.name, UserEmailKey : quote.email, UserPhoneNumberKey : quote.phone};
		[[NSUserDefaults standardUserDefaults] setObject: userInfo forKey: UserInfoKey];
		
        self.secondStepButton.enabled = YES;
    }
    else {
        self.secondStepButton.enabled = NO;
        self.submitButton.enabled = NO;
    }
    
    self.retailerName.layer.borderColor = self.retailerName.text.length > 0 ? [UIColor CNCGrayColor].CGColor : [UIColor redColor].CGColor;
}

- (IBAction)nextAction:(id)sender {
    self.thirdCoverView.hidden = YES;
    
    [self.view endEditing:YES];
}

- (IBAction)agreeAction:(UISwitch *)sender {
    [self checkSubmitButton:self.currentQuote];
    self.currentQuote.isTermsSelected = sender.isOn;
}

- (IBAction)submitAction:(UIButton *)sender {
    [MBProgressHUD showHUDAddedTo:self.parentViewController.view animated:YES];
    
    
    [[CNCApi sharedClient] createQuote:self.currentQuote withSuccess:^(CNCQuote *quote, NSError *error) {
        [MBProgressHUD hideAllHUDsForView:self.parentViewController.view animated:YES];
      
               if (error) {
            if ([self.menuDelegate respondsToSelector:@selector(didSelectFailItemWithQuote:)]) {
                [self.menuDelegate didSelectFailItemWithQuote:self.currentQuote];
            }
            
            return;
        }
        
        if ([self.menuDelegate respondsToSelector:@selector(didSelectThankYouItem)]) {
            [self.menuDelegate didSelectThankYouItem];
        }
        
    }];
}

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
});

    return YES;
}

-(void)textViewDidChange:(UITextView *)textView {
    self.currentQuote.extraInfo = textView.text;
}

- (IBAction)brandAction:(UIButton*)sender {
    [self showBrandPicker:sender];
}

- (void)showBrandPicker:(UIButton*)sender {
    
    UIBarButtonItem* barButton = [[UIBarButtonItem alloc] initWithCustomView:sender];
    
    NSString *pickerResourcePath = [[NSBundle mainBundle]pathForResource: @"WatchesBrand" ofType:@"plist"];
    NSArray* watchBrandes = [[NSArray alloc]initWithContentsOfFile:pickerResourcePath];
    
    ActionSheetStringPicker* picker = [[ActionSheetStringPicker alloc] initWithTitle:nil rows:watchBrandes initialSelection:0 doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
        self.currentQuote.brand = watchBrandes[selectedIndex];
        self.watchBrand.text = self.currentQuote.brand;
        
        [self.watchModel becomeFirstResponder];
        
    } cancelBlock:^(ActionSheetStringPicker *picker) {
        //
    } origin:barButton];
	
    [picker showActionSheetPicker];
	
	BOOL condition = ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad );
	condition = condition && [picker.toolbar respondsToSelector: @selector(setBarTintColor:)];
	
	if (condition)
	{
		picker.toolbar.barTintColor = [UIColor blackColor];
	}
}



-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    return cell;
}

- (void)termsLabelTap
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:kCNCTermsURL]];
}


-(BOOL) validateEmail: (NSString *) candidate {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];    //  return 0;
    return  ([emailTest evaluateWithObject:candidate] && candidate.length>3 && candidate.length<255);
}

#define PHONEACCEPTEDSYMBOLS @"1234567890+-()"
- (BOOL) validatePhone:(NSString*) phone
{
    NSCharacterSet *unacceptedInput = [[NSCharacterSet characterSetWithCharactersInString:PHONEACCEPTEDSYMBOLS] invertedSet];
    
    NSRange range = [phone rangeOfCharacterFromSet:unacceptedInput];
    
    return (phone.length >= 10 && phone.length <= 20 && range.location == NSNotFound);
}

- (BOOL) validateName:(NSString*) name
{
    return (name.length>0&& name.length<256);
}

- (BOOL) validateModelNumber:(NSString*) modelNumber
{
    return (modelNumber.length>2 && modelNumber.length<31);
}

- (BOOL) validateModelName:(NSString*) modelName
{
  return YES;
}

- (void) resetUserData
{
    self.imageBack.image = nil;
    self.imageFront.image = nil;
    
    self.customerEmail.text = nil;
    self.customerName.text = nil;
    self.customerPhone.text = nil;
    self.watchBrand.text = nil;
    self.watchModel.text = nil;
    self.watchModelNumber.text = nil;
    
    self.additionalInfo.text = nil;
    [self.agreeSwitch setOn:NO];
    
    self.secondStepButton.enabled = NO;
    self.submitButton.enabled = NO;
    
    self.segmentDocuments.selectedSegmentIndex = UISegmentedControlNoSegment;
    
    self.segmentUSLocation.selectedSegmentIndex = 0;
    
    self.secondCoverView.hidden = NO;
    self.thirdCoverView.hidden = NO;
    
    for (CNCTextField *textField in self.view.subviews) {
        textField.layer.borderColor = [UIColor CNCGrayColor].CGColor;
    }
    
    if (self.segmentModelFound.selectedSegmentIndex == 1) {
        self.segmentModelFound.selectedSegmentIndex = 0;
        [self modelFoundAction:self.segmentModelFound];
        
    }
    
    
    if (self.currentQuote) {
        self.currentQuote = nil;
    }
    self.currentQuote = [[CNCQuote alloc] init];
//    self.currentQuote.documents = [self.segmentDocuments titleForSegmentAtIndex:0];
    self.currentQuote.extraInfo = @"";
    
    self.currentQuote.isFullQuote = YES;
    self.currentQuote.isDomestic = YES;
    self.currentQuote.hasModelNumber = YES;
    self.currentQuote.hasRetailer = YES;
    self.currentQuote.retailerName = nil;
    self.retailerName.hidden = YES;
    self.referredByRetailerSegmentedControl.selectedSegmentIndex = 1;
    self.retailerName.text = nil;
    
}




@end
