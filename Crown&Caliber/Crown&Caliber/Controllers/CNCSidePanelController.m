//
//  CNCSidePanelController.m
//  Crown&Caliber
//
//  Created by Igor Tomych on 07/07/14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import "CNCSidePanelController.h"
#import "CNCMenuViewController.h"
#import "CNCConfig.h"
#import "CNCWelcomeController.h"
#import "CNCNewQuoteController.h"
#import "CNCWebViewController.h"
#import "CNCHowViewController.h"
#import "CNCSplashViewController.h"
#import "CNCContactsController.h"

@interface CNCSidePanelController () <CNCMenuDelegate>

@property (nonatomic, strong) UINavigationController *welcomeNavigationController;
@property (nonatomic, strong) UINavigationController *createNewQuoteNavigationController; //do not rename to newQuoteController!
@property (nonatomic, strong) UINavigationController *howNavigationController;
@property (nonatomic, strong) UINavigationController *webNavigationController;
@property (nonatomic, strong) UINavigationController *menuNavigationController;
@property (nonatomic, strong) UINavigationController *splashNavigationController;
@property (nonatomic, strong) UINavigationController *contactsNavigationController;

@property (nonatomic, strong) CNCWelcomeController* welcomeController;
@property (nonatomic, strong) CNCNewQuoteController* createNewQuoteController;
@property (nonatomic, strong) CNCWebViewController* webViewController;
@property (nonatomic, strong) CNCHowViewController* howViewController;
@property (nonatomic, strong) CNCSplashViewController *splashController;

@property (strong, nonatomic) CNCContactsController *contactsController;

@end

@implementation CNCSidePanelController

-(void) awakeFromNib
{
    CNCMenuViewController* menuController = [self.storyboard instantiateViewControllerWithIdentifier: @"leftViewController"];
    menuController.menuDelegate = self;
    [self setLeftPanel:menuController];

    self.welcomeNavigationController = [self.storyboard instantiateViewControllerWithIdentifier: @"welcomeViewController"];
    self.welcomeController = (CNCWelcomeController*)self.welcomeNavigationController.topViewController;
    self.welcomeController.menuDelegate = self;
    
   

    self.createNewQuoteNavigationController = [self.storyboard instantiateViewControllerWithIdentifier: @"newQuoteController"];
    self.createNewQuoteController = (CNCNewQuoteController* ) self.createNewQuoteNavigationController.topViewController;
    self.createNewQuoteController.menuDelegate = self;
    
    self.webNavigationController = [self.storyboard instantiateViewControllerWithIdentifier: @"webNavigationController"];
    self.webViewController = (CNCWebViewController*) self.webNavigationController.topViewController;
    self.webViewController.menuDelegate = self;
    
    self.howNavigationController = [self.storyboard instantiateViewControllerWithIdentifier: @"howNavigationController"];
    self.howViewController = (CNCHowViewController*)self.howNavigationController.topViewController;
    self.howViewController.menuDelegate = self;
    
    self.splashNavigationController = [self.storyboard instantiateViewControllerWithIdentifier:@"splashNavigationController"];
    self.splashController = (CNCSplashViewController*)self.splashNavigationController.topViewController;
    self.splashController.menuDelegate = self;
  
    self.contactsNavigationController = [self.storyboard instantiateViewControllerWithIdentifier:@"contactsViewController"];
    
    [self setCenterPanel:self.splashNavigationController];
    
    self.leftFixedWidth = 150;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)stylePanel:(UIView *)panel {
    //removing rounded corners, do not remove this method!
}

#pragma mark - Menu actions


-(void)didSelectBlogItem {
    
    [self setCenterPanel:self.webNavigationController];
    [self.webViewController loadPageFromString:kCNCBlogURL];

}

- (void)didSelectHowItem {
    [self setCenterPanel:self.howNavigationController];
}

-(void)didSelectNewQuoteItem {
    [self setCenterPanel:self.createNewQuoteNavigationController];
    [self.createNewQuoteController resetUserData];
}

- (void)didSelectTermsAndConditionsItem {
    [self setCenterPanel:self.webNavigationController];
    [self.webViewController loadPageFromString:kCNCTermsURL];
}

- (void)didSelectPrivacyPolicyItem {
    [self setCenterPanel:self.webNavigationController];
    [self.webViewController loadPageFromString:kCNCPrivacyURL];

}

- (void)didSelectContactsItem {
    [self setCenterPanel:self.contactsNavigationController];
}

- (void)didSelectThankYouItem {
    [self setCenterPanel:self.welcomeNavigationController];
    [self.welcomeController loadSuccessScreen];
}

- (void)didSelectFailItemWithQuote:(CNCQuote*)quote {
    [self setCenterPanel:self.welcomeNavigationController];
    [self.welcomeController loadFailScreen];
    self.welcomeController.currentQuote = quote ;
}

- (void)didSelectWelcomeItem {
  [self loadWelcomeScreen];
}


- (void)loadWelcomeScreen{
    [self setCenterPanel:self.welcomeNavigationController];
    [self.welcomeController loadWelcomeScreen];
}

-(NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

@end
