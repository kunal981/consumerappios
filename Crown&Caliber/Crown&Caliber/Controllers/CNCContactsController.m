//
//  CNCContactsController.m
//  Crown&Caliber
//
//  Created by Igor Tomych on 29/07/14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import "CNCContactsController.h"

@interface CNCContactsController ()

@end

@implementation CNCContactsController

- (void)viewDidLoad
{
  [super viewDidLoad];
    self.title = @"Contacts";

  self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStyleBordered target:nil action:nil];
}

- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];

}


@end
