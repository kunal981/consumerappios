//
//  CNCMenuViewController.h
//  Crown&Caliber
//
//  Created by Igor Tomych on 07/07/14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNCQuote.h"

@protocol CNCMenuDelegate <NSObject>

- (void)didSelectWelcomeItem;
- (void)didSelectNewQuoteItem;
- (void)didSelectBlogItem;
- (void)didSelectHowItem;
- (void)didSelectTermsAndConditionsItem;
- (void)didSelectPrivacyPolicyItem;
- (void)didSelectThankYouItem;
- (void)didSelectContactsItem;
- (void)didSelectFailItemWithQuote:(CNCQuote*)quote;
- (void)loadWelcomeScreen;

@end

@interface CNCMenuViewController : UITableViewController

@property (nonatomic, weak) id<CNCMenuDelegate> menuDelegate;

@end
