//
//  CNCTabBarController.h
//  Crown&Caliber
//
//  Created by Igor Tomych on 07/07/14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CNCTabBarController : UITabBarController

-(void) hideFooter:(BOOL)hide;

@end
