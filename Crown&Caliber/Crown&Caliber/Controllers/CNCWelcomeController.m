//
//  CNCWelcomeController.m
//  Crown&Caliber
//
//  Created by Igor Tomych on 07/07/14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import "CNCWelcomeController.h"
#import "CNCApi.h"
#import <MBProgressHUD.h>
#import "CNCCreateQuoteViewController.h"

@interface CNCWelcomeController ()

@property (nonatomic, weak) IBOutlet UIImageView *imageView;
@property (nonatomic, weak) IBOutlet UIImageView *logoImageView;
@property (nonatomic, weak) IBOutlet UIButton *button;
@property (nonatomic, weak) IBOutlet UILabel *textLabel;
@property (nonatomic, weak) IBOutlet UILabel *thankYouTextLabel;
@property (nonatomic) BOOL failScreenIsLoaded;

@property (strong, nonatomic) IBOutlet UITextView *phoneTextView;
@property (strong, nonatomic) IBOutlet UITextView *emailTextView;
@property (strong, nonatomic) IBOutlet UITextView *webTextView;


@end

@implementation CNCWelcomeController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


- (void)loadWelcomeScreen
{
    self.imageView.image = [UIImage imageNamed:@"cover_landing"];
    self.logoImageView.image = [UIImage imageNamed:@"logo.png"];
    self.logoImageView.hidden = NO;
    self.textLabel.text = @"";
    [self.button setTitle:@"Get a Quote" forState:UIControlStateNormal];
    
    self.failScreenIsLoaded = NO;
    
    [self.thankYouTextLabel setHidden:YES];
    [self.phoneTextView setHidden:YES];
    [self.emailTextView setHidden:YES];
    [self.webTextView setHidden:YES];
    
    for ( int tag = 100; tag < 105; tag++ ) { //FIXME: IPAD Dirty Hack. Outlets appear to be empty
        [[self.view viewWithTag: tag] setHidden:YES];
    }
}

- (void) loadSuccessScreen
{
    [self.logoImageView setHidden:YES];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
         self.imageView.image = [UIImage imageNamed:@"cover_NewQuote_ThankYou.png"];
    }
   
    self.textLabel.text = @"A Crown & Caliber valuation expert will review your information and will then contact you with a free quote within 1-3 business days, depending on the rarity of your watch. In the meantime, please contact us if you have any questions.";
    [self.button setTitle:@"Get another Quote" forState:UIControlStateNormal];
    self.failScreenIsLoaded = NO;
    
    [self.thankYouTextLabel setHidden:NO];
    [self.phoneTextView setHidden:NO];
    [self.emailTextView setHidden:NO];
    [self.webTextView setHidden:NO];
    
    for ( int tag = 100; tag < 105; tag++ ) { //FIXME: IPAD Dirty Hack. Outlets appear to be empty
        [[self.view viewWithTag: tag] setHidden:NO];
    }
}

- (void) loadFailScreen
{
    [self.logoImageView setHidden:YES];
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        self.imageView.image = [UIImage imageNamed:@"cover_NewQuote_ThankYou.png"];
    }
    self.textLabel.text = @"An error occurred. Please try again";
    [self.button setTitle:@"Try Again" forState:UIControlStateNormal];
    [self.thankYouTextLabel setHidden:YES];
    self.failScreenIsLoaded = YES;
}

-(IBAction)createNewQuoteButtonPressed:(id)sender
{
    if (self.failScreenIsLoaded) {
        __weak CNCWelcomeController *weakSelf = self;
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[CNCApi sharedClient] createQuote:self.currentQuote withSuccess:^(CNCQuote *quote, NSError *error) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            if (error) {
                [weakSelf loadFailScreen];
            }
            else
            {
                [weakSelf loadSuccessScreen];
            }
        }];
    }
    else
    {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            [self.tabBarController setSelectedIndex:0];
            [self loadSuccessScreen];
            CNCCreateQuoteViewController *controller = (CNCCreateQuoteViewController*)self.tabBarController.selectedViewController;
            [controller createNewQuote];
            
        }
        else {
            if ([self.menuDelegate respondsToSelector:@selector(didSelectNewQuoteItem)]) {
                [self.menuDelegate didSelectNewQuoteItem];
            }
        }
    }
}




@end
