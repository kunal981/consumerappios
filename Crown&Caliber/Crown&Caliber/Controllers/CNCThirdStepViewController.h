//
//  CNCThirdStepViewController.h
//  Crown&Caliber
//
//  Created by valerio8 on 09.07.14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CNCQuote.h"
@protocol CNCThirdStepDelegate;
@interface CNCThirdStepViewController : UIViewController
@property (strong,nonatomic) CNCQuote *currentQuote;
@property (strong,nonatomic) id<CNCThirdStepDelegate> delegate;

-(void) deleteUserData;

@end

@protocol CNCThirdStepDelegate <NSObject>

-(void) goToSuccessScreen;

@end