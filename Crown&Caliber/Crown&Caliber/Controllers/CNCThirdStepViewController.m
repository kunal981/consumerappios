//
//  CNCThirdStepViewController.m
//  Crown&Caliber
//
//  Created by valerio8 on 09.07.14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import "CNCThirdStepViewController.h"
#import "UIColor+CNCColor.h"
#import "CNCApi.h"
#import "CNCTextField.h"
#import <JVFloatLabeledTextView.h>
#import "CNCConfig.h"

#define ANIMATION_SPEED 0.5
@interface CNCThirdStepViewController () <UITextFieldDelegate, UIGestureRecognizerDelegate>


@property (nonatomic,weak) IBOutlet JVFloatLabeledTextView *additionalInfoTextView;

@property (nonatomic,weak) IBOutlet UIButton *submitButton;
@property (nonatomic,weak) IBOutlet UILabel *termsLabel;
@property (nonatomic,weak) IBOutlet UISwitch *agreeSwitch;

@property (nonatomic,weak) IBOutlet UIView *viewToMove;

@property (nonatomic,strong)  UITapGestureRecognizer *termsTap;
@property (nonatomic,weak) IBOutlet CNCTextField *retailerNameTextField;
@property (nonatomic,weak) IBOutlet UISegmentedControl *retailerSegmentedControl;




@end

@implementation CNCThirdStepViewController

typedef NS_ENUM(NSUInteger, CNCSecondStepField){
    CNCAssociateName = 1,
    CNCWhoName
};

- (void)viewDidLoad
{
    [super viewDidLoad];

    
    self.additionalInfoTextView.layer.cornerRadius = 0.0f;
    self.additionalInfoTextView.layer.masksToBounds = YES;
    self.additionalInfoTextView.layer.borderColor = [[UIColor CNCGrayColor] CGColor];
    self.additionalInfoTextView.layer.borderWidth = 1.0f;
    
    self.additionalInfoTextView.placeholder = @"Additional information";
    
    self.termsTap =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(termsLabelTap)];
    self.termsTap.numberOfTapsRequired = 1;
    self.termsTap.delegate = self;
    [self.termsLabel addGestureRecognizer:self.termsTap];
    
    NSMutableAttributedString * string = [[NSMutableAttributedString alloc] initWithString:@"I am over 18 and agree to the terms & conditions"];

    [string addAttribute:NSForegroundColorAttributeName value:[UIColor blueColor] range:NSMakeRange(30  ,18)];
    [string  addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInt:NSUnderlineStyleSingle] range:NSMakeRange(30, 18)];
    
    self.termsLabel.attributedText = string;
    
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


-(IBAction)submitButtonPressed:(id)sender
{
    [self.view endEditing:YES];
    if (self.agreeSwitch.isOn) {
       [self.delegate goToSuccessScreen];
       
    }
}


-(IBAction)agreeSwitchAction:(id)sender
{
    //[self.agreeSwitch setOn:!self.agreeSwitch.isOn];
    [self checkSubmitButtonAvailability:self.currentQuote];
    self.currentQuote.isTermsSelected = self.agreeSwitch.isOn;
}

- (void)deleteUserData
{
    self.additionalInfoTextView.text = nil;

    [self.agreeSwitch setOn:NO];
    
    self.submitButton.enabled = NO;
    
    if (self.retailerSegmentedControl.selectedSegmentIndex == 0)
    {
        [self animateRetailerTextFieldWithDirectionUp:YES];
        self.retailerSegmentedControl.selectedSegmentIndex = 1;
    }
    
    self.currentQuote.hasRetailer = NO;
    self.currentQuote.retailerName = nil;
    self.retailerNameTextField.text = nil;

    
}
- (void)checkSubmitButtonAvailability:(CNCQuote*)quote {
    if (self.agreeSwitch.isOn && ((self.retailerSegmentedControl.selectedSegmentIndex == 0)? (quote.retailerName.length > 0):YES)) {
        self.submitButton.enabled = YES;
    }
    else
    {
        self.submitButton.enabled = NO;
    }
    
    self.retailerNameTextField.layer.borderColor = self.retailerNameTextField.text.length > 0 ? [UIColor CNCGrayColor].CGColor : [UIColor redColor].CGColor;
}

- (void)termsLabelTap
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:kCNCTermsURL]];
}

-(void)textViewDidChange:(UITextView *)textView {
    self.currentQuote.extraInfo = textView.text;
}

- (IBAction)textChanged:(JVFloatLabeledTextField *)sender {

    if (sender == self.retailerNameTextField)
    {
        self.currentQuote.retailerName = sender.text;
    }
    [self checkSubmitButtonAvailability:self.currentQuote];
}



-(IBAction)segmentedControlerChanged:(id)sender
{
    UISegmentedControl *segmentedControl = sender;
    
    if (segmentedControl.selectedSegmentIndex == 0)
    {
        [self animateRetailerTextFieldWithDirectionUp:NO];
        self.currentQuote.hasRetailer = YES;
    }
    else
    {
        [self animateRetailerTextFieldWithDirectionUp:YES];
        self.currentQuote.hasRetailer = NO;
    }
    
    [self checkSubmitButtonAvailability:self.currentQuote];
    
}

-(void) animateRetailerTextFieldWithDirectionUp:(BOOL) directionUp
{
    CGRect frame =self.viewToMove.frame;
    float moveHeight = 60;
    frame.origin.y += (directionUp)? - moveHeight : moveHeight;

    [UIView animateWithDuration:ANIMATION_SPEED animations:^{
        self.viewToMove.frame = frame;
        [self.retailerNameTextField setHidden:directionUp];
    } completion:^(BOOL finished) {
        
    }];
}

@end
