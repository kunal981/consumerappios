//
//  CNCCreateQuoteViewController.h
//  Crown&Caliber
//
//  Created by Igor Tomych on 08/07/14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CNCCreateQuoteViewController : UIViewController <UIScrollViewDelegate>

-(void) createNewQuote;
@end
