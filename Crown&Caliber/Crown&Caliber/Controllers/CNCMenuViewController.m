//
//  CNCMenuViewController.m
//  Crown&Caliber
//
//  Created by Igor Tomych on 07/07/14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import "CNCMenuViewController.h"
#import "CNCMenuTableViewCell.h"
#define menuItems @[  \
@{@"title":@"Home", @"menuImage": @"icon_home_gray.png", @"menuImageSelected":@"icon_home_black.png"}, \
@{@"title":@"Get a Quote", @"menuImage": @"", @"menuImageSelected":@""}, \
@{@"title":@"Blog", @"menuImage": @"ipad_icon_book.png", @"menuImageSelected":@"ipad_icon_book_black.png"}, \
@{@"title":@"How", @"menuImage": @"ipad_icon_video.png", @"menuImageSelected":@"ipad_icon_video_black.png"}, \
@{@"title":@"Terms & Conds. ", @"menuImage": @"ipad_icon_clipboard.png", @"menuImageSelected":@"ipad_icon_clipboard_black.png"}, \
@{@"title":@"Privacy Policy", @"menuImage": @"ipad_icon_eye.png", @"menuImageSelected":@"ipad_icon_eye_black.png"}, \
@{@"title":@"Contacts", @"menuImage": @"icon_book_gray", @"menuImageSelected":@"icon_contacts_black.png" }]

@interface CNCMenuViewController ()

@property (strong, nonatomic) IBOutlet UIView *sectionHeader;
@property (nonatomic) NSInteger currentIndex;
@property (strong, nonatomic) IBOutlet UIView *footerView;

@end

@implementation CNCMenuViewController

typedef NS_ENUM(NSUInteger, CNCMenuItem){
  CNCMenuSectionHome,
  CNCMenuSectionNewQuote,
  CNCMenuSectionBlog,
  CNCMenuSectionHow,
  CNCMenuSectionTerms,
  CNCMenuSectionPrivacy,
  CNCMenuSectionContacts,
  CNCMenuTotalCount
};


- (void)viewDidLoad
{
  [super viewDidLoad];
  self.tableView.tableFooterView = self.footerView;
}

- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
  
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
  return CNCMenuTotalCount;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
  return self.sectionHeader;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
  self.currentIndex = indexPath.row;
  [self.tableView reloadData];
  
  if ([self.menuDelegate respondsToSelector:@selector(didSelectWelcomeItem)] && indexPath.row == CNCMenuSectionHome) {
    [self.menuDelegate didSelectWelcomeItem];
  }
  if ([self.menuDelegate respondsToSelector:@selector(didSelectNewQuoteItem)] && indexPath.row == CNCMenuSectionNewQuote) {
    [self.menuDelegate didSelectNewQuoteItem];
  }
  else if ([self.menuDelegate respondsToSelector:@selector(didSelectHowItem)] && indexPath.row == CNCMenuSectionHow) {
    [self.menuDelegate didSelectHowItem];
  }
  else if ([self.menuDelegate respondsToSelector:@selector(didSelectBlogItem)] && indexPath.row == CNCMenuSectionBlog) {
    [self.menuDelegate didSelectBlogItem];
  }
  else if ([self.menuDelegate respondsToSelector:@selector(didSelectPrivacyPolicyItem)] && indexPath.row == CNCMenuSectionPrivacy) {
    [self.menuDelegate didSelectPrivacyPolicyItem];
  }
  else if ([self.menuDelegate respondsToSelector:@selector(didSelectContactsItem)] && indexPath.row == CNCMenuSectionContacts) {
    [self.menuDelegate didSelectContactsItem];
  }
  
  else if ([self.menuDelegate respondsToSelector:@selector(didSelectTermsAndConditionsItem)] && indexPath.row == CNCMenuSectionTerms) {
    [self.menuDelegate didSelectTermsAndConditionsItem];
  }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
  CNCMenuTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:indexPath.row == CNCMenuSectionNewQuote?@"newQuoteCell":@"customCell" forIndexPath:indexPath];
  
  [cell configureCellWithDictionary:menuItems[indexPath.row] isSelected:self.currentIndex == indexPath.row isFirstItem:indexPath.row == CNCMenuSectionNewQuote];
  
  return cell;
  
}

@end
