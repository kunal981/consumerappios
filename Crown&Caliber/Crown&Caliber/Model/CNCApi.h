//
//  CNCApi.h
//  Crown&Caliber
//
//  Created by Igor Tomych on 07/07/14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CNCQuote.h"

@interface CNCApi : NSObject

+ (instancetype)sharedClient;


- (void)createQuote:(CNCQuote*)quote withSuccess:(void (^)(CNCQuote *quote, NSError* error))success;

@end
