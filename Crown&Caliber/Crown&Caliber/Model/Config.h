//
//  Config.h
//  Crown&Caliber
//
//  Created by Igor Tomych on 11/07/14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#ifndef Crown_Caliber_Config_h
#define Crown_Caliber_Config_h


#define VIDEO1_LINK @"https://88acf46140a46211eb81-899ad5f3af188918b2be03c0f100d398.ssl.cf5.rackcdn.com/ios/C&C%20STORY%20iPhone%20Retina%20Version.mp4"
#define VIDEO2_LINK @"https://88acf46140a46211eb81-899ad5f3af188918b2be03c0f100d398.ssl.cf5.rackcdn.com/ios/C&C%20STORY%20iPad%20Version.mp4"
#define VIDEO3_LINK @"https://88acf46140a46211eb81-899ad5f3af188918b2be03c0f100d398.ssl.cf5.rackcdn.com/ios/C&C%20STORY%20iPad%20Retina%20Version.mp4"
#define VIDEO4_LINK @"https://88acf46140a46211eb81-899ad5f3af188918b2be03c0f100d398.ssl.cf5.rackcdn.com/ios/C&C%20STORY%20iPhone%20Version.mp4"

#ifndef DEBUG

    #define NSLog(...)

#endif

#endif
