//
//  CNCApi.m
//  Crown&Caliber
//
//  Created by Igor Tomych on 07/07/14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import "CNCApi.h"
#import <AFNetworking/AFNetworking.h>

//#define API_HOST @"https://crown.sandbox.rietta.com/api/external/quote_requests.json"
#define API_HOST @"https://portal.crownandcaliber.com/api/external/quote_requests.json"
//#define API_TOKEN @"9bd757e9bcca4e8bba4d6ccd1356929e:7156e3acce1f12152587ea636c8826af95335a880275d579e4e69393c5d604bbae54b4122a59cb7dd4cc697657de0a6caa910cfe791459af"
#define API_TOKEN @"b48e7e6f69cb4f2bad36d38cbfcded7c:491ce05faded67ac6c4b04e044d2503da2df6b8fde8113082a96eefe9fb9ff6861e1af155e7c0a258172a9e898cbb6ee19b82e4b7f66970d"
#define API_USERAGENT @"iphone"
#define API_IP @"127.0.0.1"
#define API_SOURCE @"ios"



@implementation CNCApi

+(instancetype)sharedClient {
  static CNCApi *_sharedClient = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    _sharedClient = [[self alloc] init];
    
  });
  
  return _sharedClient;
}

-(void)createQuote:(CNCQuote *)quote withSuccess:(void (^)(CNCQuote *quote, NSError *error))success {
  AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
  
  
  
  NSMutableDictionary* params = [NSMutableDictionary dictionaryWithDictionary:@{@"api_token": API_TOKEN, \
                                                                                @"name" : quote.name, \
                                                                                @"phone" : quote.phone, \
                                                                                @"email" : quote.email, \
                                                                                @"brand" : quote.brand, \
                                                                                @"documents" : quote.documents, \
                                                                                @"extra_info" : quote.extraInfo, \
                                                                                @"domestic" : quote.isDomestic?@"true":@"false", \
                                                                                @"no_model_number" : quote.hasModelNumber?@"false":@"true", \
                                                                                @"retailer" : quote.retailerName?@"true":@"false", \
																				@"full_quote" : quote.isFullQuote?@"true":@"false",\
                                                                                @"terms" : @"true", \
                                                                                @"originating_ip" : API_IP, \
                                                                                @"originating_source" : API_SOURCE, \
                                                                                @"originating_user_agent" : API_USERAGENT }];
  
  if (quote.modelName.length > 0) {
    [params setObject:quote.modelName forKey:@"model_name"];
  }
  
  if (quote.modelNumber.length > 0) {
    [params setObject:quote.modelNumber forKey:@"model_number"];
  }
  
  if (quote.retailerName.length > 0) {
    [params setObject:quote.retailerName forKey:@"retailer_name"];
  }
  
  
  [manager POST:API_HOST parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
    if ([responseObject isKindOfClass:[NSDictionary class]] && responseObject[@"success"]) {
      
      quote.imagesPostURLString = responseObject[@"attachments_post_url"];
      quote.caseId = responseObject[@"case_id"];
      quote.sessionId = responseObject[@"session_id"];
      
      [self uploadImage:quote.imageFront toQuote:quote withSuccess:^(CNCQuote *quote, NSError *error) {
        [self uploadImage:quote.imageBack toQuote:quote withSuccess:^(CNCQuote *quote, NSError *error) {
          if (success) {
            success(quote, nil);
          }
        }];
      }];
      
    }
    
  } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
    if (success) {
      success(quote, error);
    }
  }];
}

- (void)uploadImage:(UIImage*)image toQuote:(CNCQuote*)quote  withSuccess:(void (^)(CNCQuote *quote, NSError *error))success {
  
  
  NSData *imageData = UIImageJPEGRepresentation(image, 0.8);
  
  AFHTTPRequestSerializer *serializer = [AFHTTPRequestSerializer serializer];
  
  NSError* error;
  
  NSMutableURLRequest *request = [serializer multipartFormRequestWithMethod:@"POST" URLString:quote.imagesPostURLString  parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
    [formData appendPartWithFormData:[[quote.sessionId  stringByAppendingString:@";type=text"] dataUsingEncoding:NSUTF8StringEncoding] name:@"session_id"];
    [formData appendPartWithFormData:[[API_TOKEN  stringByAppendingString:@";type=text"] dataUsingEncoding:NSUTF8StringEncoding] name:@"api_token"];
    [formData appendPartWithFileData:imageData name:@"attachment[data]" fileName:@"myimage.jpg" mimeType:@"image/jpeg"];
  } error:&error];
  
  [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
  request.timeoutInterval = 180;
  
  
  AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
  AFHTTPRequestOperation *operation = [manager HTTPRequestOperationWithRequest:request success:^(AFHTTPRequestOperation *operation, id responseObject) {
    NSLog(@"Success %@", responseObject);
    
    if (success) {
      success(quote, nil);
    }
  }
                                                                       failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                                                         NSLog(@"Failure %@", error.description);
                                                                         
                                                                         if (success) {
                                                                           success(quote, error);
                                                                         }
                                                                       }];
  
  [operation setUploadProgressBlock:^(NSUInteger __unused bytesWritten,
                                      long long totalBytesWritten,
                                      long long totalBytesExpectedToWrite) {
    NSLog(@"Wrote %lld/%lld", totalBytesWritten, totalBytesExpectedToWrite);
  }];
  
  [operation start];
  
  
  
  
}

@end
