//
//  CNCQuote.h
//  Crown&Caliber
//
//  Created by Igor Tomych on 07/07/14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSString* const UserInfoKey = @"userInfo";
static NSString* const UserNameKey = @"userName";
static NSString* const UserEmailKey = @"userEmail";
static NSString* const UserPhoneNumberKey = @"userPhoneNumber";

@interface CNCQuote : NSObject

@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* phone;
@property (nonatomic, strong) NSString* email;
@property (nonatomic, strong) NSString* brand;
@property (nonatomic, strong) NSString* modelName;
@property (nonatomic, strong) NSString* modelNumber;
@property (nonatomic, strong) NSString* documents;
@property (nonatomic, strong) NSString* extraInfo;
@property (nonatomic, strong) NSString* retailerName;
@property (nonatomic, strong) NSString* retailerAssociateName;


@property (nonatomic, strong) NSString* caseId;
@property (nonatomic, strong) NSString* sessionId;
@property (nonatomic, strong) NSString* imagesPostURLString;

@property (nonatomic, strong) UIImage* imageFront;
@property (nonatomic, strong) UIImage* imageBack;

@property (nonatomic) BOOL isDomestic;
@property (nonatomic) BOOL hasModelNumber;
@property (nonatomic) BOOL hasRetailer;
@property (nonatomic) BOOL isTermsSelected;
@property (nonatomic) BOOL isFullQuote;

@end
