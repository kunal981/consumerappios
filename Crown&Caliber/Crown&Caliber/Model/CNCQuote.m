//
//  CNCQuote.m
//  Crown&Caliber
//
//  Created by Igor Tomych on 07/07/14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import "CNCQuote.h"

@implementation CNCQuote

- (instancetype)initWithBaseURL:(NSURL *)url {
    self = [super init];
    if (!self) {
        return nil;
        
    }
    
    self.extraInfo = @"";
    self.isTermsSelected = YES;
    self.isDomestic = YES;
    return self;
}

- (void)setImageFront:(UIImage *)imageFront {
  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
    _imageFront = [self resizeImage:imageFront];
  });
}


- (void)setImageBack:(UIImage *)imageBack {
  dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
    _imageBack = [self resizeImage:imageBack];
  });
}

#pragma mark - Image resizing

-(UIImage*) resizeImage:(UIImage*) imageToResize{
    CGSize  dimensions = [imageToResize size];
    if (dimensions.width > 1024){
        int resizeImageWidth = 1024;
        int resizedImageHeight = (resizeImageWidth * dimensions.height) / dimensions.width;
        UIGraphicsBeginImageContext(CGSizeMake(resizeImageWidth, resizedImageHeight));
        [imageToResize drawInRect: CGRectMake(0, 0, resizeImageWidth, resizedImageHeight)];
        UIImage *resizedImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        return resizedImage;
    }
    return imageToResize;
}

@end
