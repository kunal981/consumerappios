//
//  CNCTextField.m
//  Crown&Caliber
//
//  Created by valerio8 on 11.07.14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import "CNCTextField.h"
#import "UIColor+CNCColor.h"

@implementation CNCTextField

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.layer.cornerRadius = 0.0f;
    self.layer.masksToBounds = YES;
    [self.layer setBorderColor:[[UIColor CNCGrayColor] CGColor]];
    
    self.tintColor = [UIColor blackColor];
    
    self.layer.borderWidth = 1.0f;
}


- (CGRect)textRectForBounds:(CGRect)bounds {
    return CGRectInset( bounds , 10 , 0 );
}

-(CGRect)editingRectForBounds:(CGRect)bounds {
    return CGRectInset( bounds , 10 , 0 );
}

@end
