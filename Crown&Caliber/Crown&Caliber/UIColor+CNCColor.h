//
//  UIColor+CNCColor.h
//  Crown&Caliber
//
//  Created by valerio8 on 09.07.14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (CNCColor)

+ (UIColor *)CNCGrayColor;
+ (UIColor *)CNCOrangeColor;

@end
