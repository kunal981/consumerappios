//
//  CNCTextField.h
//  Crown&Caliber
//
//  Created by valerio8 on 11.07.14.
//  Copyright (c) 2014 SROST Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <JVFloatLabeledTextField.h>

@interface CNCTextField : JVFloatLabeledTextField

@end
